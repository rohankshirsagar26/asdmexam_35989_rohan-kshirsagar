const express = require("express");
const db = require("./db");
const parser = require("body-parser");
const app = express();
app.use(parser.json());

//GET Method
app.get("/", (req, res) => {
  const stmt = `select * from emp`;
  db.query(stmt, (error, result) => {
    if (error) {
      res.send({ status: "error", data: error });
    } else {
      res.send({ status: "success", data: result });
    }
  });
});
app.listen(4000, () => {
  console.log("server started on 4000");
});

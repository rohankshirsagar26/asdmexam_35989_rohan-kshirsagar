const mysql = require("mysql2");

const pool = mysql.createPool({
  host: "192.168.31.134",
  user: "root",
  password: "root",
  database: "empdb",
  port: 9090,
  connectionLimit: 20,
});
